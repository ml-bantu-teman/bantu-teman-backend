from django.shortcuts import render
from django.http import JsonResponse
import tweepy
from os import environ

from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.keys import Keys
import os
import time

consumer_key = environ['consumer_key']
consumer_secret = environ['consumer_secret']
access_token = environ['access_token']
access_token_secret = environ['access_token_secret']

auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
auth.set_access_token(access_token, access_token_secret)

api = tweepy.API(auth, wait_on_rate_limit=True, wait_on_rate_limit_notify= True)

def get_user_tweet(username):
    tweets = api.user_timeline(screen_name=username,
                               count=200,
                               include_rts=False,
                               tweet_mode="extended"
                               )
    list_tweet = []

    i = 1
    for tweet in tweets:
        list_tweet.append(str(tweet.full_text))

        i += 1
        if i == 5:
            break
    return list_tweet

def get_user_followers(username):
    followers = tweepy.Cursor(api.followers, screen_name=username).items()
    list_follower = []

    i = 1
    for user in followers:
        list_follower.append(str(user.screen_name))

        i += 1
        if i == 10:
            break
    return list_follower

def get_user_following(username):
    followings = tweepy.Cursor(api.friends, screen_name=username).items()
    list_following = []

    i = 1
    for user in followings:
        list_following.append(str(user.screen_name))

        i += 1
        if i == 60:
            break
    return list_following


def API_get_user_following(request, username):

    json_list_following = {
        "followings" : get_user_following(username)
    }
    return JsonResponse(json_list_following)

def API_get_user_tweet(request, username):

    json_list_tweet = {
        "tweet" : get_user_tweet(username)
    }
    return JsonResponse(json_list_tweet)

def API_get_user_followers(request, username):


    json_list_followers = {
        "followers" : get_user_followers(username)
    }
    return JsonResponse(json_list_followers)

def manual_tweets_scrapper_by_username(username):
    list_twitter = []
    chrome_options = webdriver.ChromeOptions()
    chrome_options.binary_location = os.environ.get("GOOGLE_CHROME_BIN")
    chrome_options.add_argument("--headless")
    chrome_options.add_argument("--disable-dev-shm-usage")
    chrome_options.add_argument("--no-sandbox")
    browser = webdriver.Chrome(executable_path=os.environ.get("CHROMEDRIVER_PATH"), chrome_options=chrome_options)
    
    twitter_username = username
    browser.get("https://twitter.com/" + twitter_username)

    time.sleep(1)

    elem = browser.find_element_by_tag_name("body")

    scroll_flag = 25

    while scroll_flag:
        elem.send_keys(Keys.PAGE_DOWN)
        time.sleep(0.5)
        scroll_flag-=1

    twitter_elm = browser.find_elements_by_class_name("tweet")

    for post in twitter_elm:
        username = post.find_element_by_class_name("username")
        if username.text.lower() == "@" + twitter_username.lower():
            tweet = post.find_element_by_class_name("tweet-text")
            list_twitter.append(str(tweet.text))

    browser.quit()
    return list_twitter
    

def API_get_user_tweet_manual(request, username):
    json_list_tweet = {
        "tweet" : manual_tweets_scrapper_by_username(username)
    }
    return JsonResponse(json_list_tweet)