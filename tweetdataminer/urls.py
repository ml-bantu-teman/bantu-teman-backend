from django.urls import path

from . import views

urlpatterns = [
    path('getfollowing/<str:username>', views.API_get_user_following),
    path('gettweets/<str:username>', views.API_get_user_tweet),
    path('getfollowers/<str:username>', views.API_get_user_followers),
    path('gettweets-manual/<str:username>', views.API_get_user_tweet_manual)

]