from django.apps import AppConfig


class TweetdataminerConfig(AppConfig):
    name = 'tweetdataminer'
