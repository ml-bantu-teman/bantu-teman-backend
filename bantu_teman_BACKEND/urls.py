from django.contrib import admin
from django.urls import include, path
from rest_framework import routers
from landing import views

router = routers.DefaultRouter()

# router.register(r'users', views.UserViewSet)
# router.register(r'groups', views.GroupViewSet)
router.register(r'words/all', views.WordAllView, basename='words/all')
router.register(r'words/add', views.WordAddView, basename='words/add')
# router.register(r'coba', views.CobaViewSet, basename='coba') # CUMA BUAT COBA COBA
router.register(r'words/count', views.WordCountView, basename='words/count') # CUMA BUAT COBA COBA
# router.register(r'nomodel', views.YourView, basename='nomodel') # CUMA BUAT COBA COBA
# router.register(r'testcount', views.WordCountView, basename='testcount') # CUMA BUAT COBA COBA

# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.
urlpatterns = [
    path('', views.api_list),
    path('api/', include(router.urls)),
    path('admin/', admin.site.urls),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    path('twitterdataminer/', include('tweetdataminer.urls')),
    path('store-tweets-datas/', views.store_tweets_data),
    path('coba/', views.coba),
    path('coba-dua/', views.coba_dua),
    path('coba-tiga/', views.coba_tiga),
    path('sentiment-analysis/<str:username>', views.API_get_following_and_probability),
    path('sentiment-analysis/single/<str:username_following>', views.get_one_following_and_probability),
    path('mock-sentiment-analysis/', views.mock_API_get_following_and_probability),
    path('sentiment-analysis/tag-from-tweets/', views.api_get_tag_from_list_of_tweets),
    path('sentiment-analysis/tag-from-sentence/<str:sentence>', views.API_get_tag_from_sentence),
    path('accuracy-checker/<int:firstRow>/<int:lastRow>', views.accuracy_checker),
    path('accuracy-experimental/<int:firstRow>/<int:lastRow>/<int:arrSize>/<int:randomCount>', views.accuracy_checker_random)



    # path('test-classifier/', views.classifier),
    # path('pasya/', views.testdistinct),
    # path('api/words/count/<str:the_word>', views.WordCountView.as_view())
]
