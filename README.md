## Bantu Teman Backend

A Sentiment Analysis app that uses Machine Learning and Naive Bayes Algorithm.

About the app : 
[https://www.youtube.com/watch?v=tgikqWfsiEw](https://www.youtube.com/watch?v=tgikqWfsiEw)

### The Backend app : 
1. [https://api-bantuteman.herokuapp.com/](https://api-bantuteman.herokuapp.com/)
2. [https://api-bantutemanv2.herokuapp.com/](https://api-bantutemanv2.herokuapp.com/)

The first app is using PostgreSQL and the second app is using SQLite3

Made with Django.

Backend Repository for http://mesinlerning2019.herokuapp.com/
Frontend Repository :
https://gitlab.com/ml-bantu-teman/bantu-teman-frontend

Made By:

* Kevin Raikhan Zain (https://gitlab.com/kevinraikhan)
* Muhammad Adipashya Yarzuq (https://gitlab.com/Falkro)
* Muhammad Feril Bagus P. (https://gitlab.com/feril68)
* Stefanus Khrisna A. H. (https://gitlab.com/khrisnaa3)