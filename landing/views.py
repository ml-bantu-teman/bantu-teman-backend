# TODO
# Refactor word.lower(), too many word.lower()

import csv
import json
import re
import string
import requests
from random import randint

from django.http import JsonResponse, HttpResponse
from django.shortcuts import render
# from nltk.corpus import stopwords
from django.views.decorators.csrf import csrf_exempt
from rest_framework import viewsets, mixins

from tweetdataminer.views import get_user_following
from tweetdataminer.views import get_user_tweet
from .models import Word
from .serializers import WordSerializer, WordPostSerializer

# nltk.download('stopwords')

# class WordCountView(APIView):
#     renderer_classes = [JSONRenderer]
#
#     def get(self, request, format=None):
#         user_count = 24
#         content = {'user_count': user_count}
#         return Response(content)

# 0 = Negative
# 1 = Positive

# Global Variable
positive_word_count = len(Word.objects.filter(tag="1"))
negative_word_count = len(Word.objects.filter(tag="0"))
amount_of_possible_word = Word.objects.values('word').distinct().count()


# stop_words = set(stopwords.words('english'))


# # Naive Bayes Classifier Algorithm
def get_amount_of_positive_words(word):
    result = 0
    print("speedTest START")
    # if word not in stop_words:
    try:
        result = Word.objects.get(word=word, tag='1').count
    except Word.DoesNotExist:

        print("Masuk EXCEPT POSITIVE " + word)
        pass
    print("speedTest END")
    return result


def get_amount_of_negative_words(word):
    result = 0
    print("speedTest START")
    # if word not in stop_words:
    try:
        result = Word.objects.get(word=word, tag='0').count
    except Word.DoesNotExist:
        print("Masuk EXCEPT NEGATIVE " + word)
        pass
    print("speedTest END\n=============")
    return result


def get_amount_of_all_possible_words():
    return amount_of_possible_word


def calculate_bayes_probability(word_count, word_per_tag_count):
    print("called")
    return (word_count + 1) / (word_per_tag_count + get_amount_of_all_possible_words())


# This function take a sentence(String) and will return the probability of that string(dictionary)
# Example:
# sentence = 'aku senang'
# return = {'positive' : 0.09, 'negative' : 0.00005}
def calculate_sentence_tag_probability(sentence):
    tag_result = {
        'positive': 0,
        'negative': 0,
        # 'tweet_word_list' : [] # Experimental Learn
    }
    # https://monkeylearn.com/blog/practical-explanation-naive-bayes-classifier/
    sentence_split = remove_punctuation(sentence)
    # tag_result['tweet_word_list'] = sentence_split # Experimental Learn
    positive_tag_probability = 1
    negative_tag_probability = 1
    for word in sentence_split:
        # calculated_positive_probability_value = 1
        # calculated_negative_probability_value = 1
        # print(word)
        # try:
        word_lowered = word.lower()
        calculated_positive_probability_value = calculate_bayes_probability(get_amount_of_positive_words(word_lowered),
                                                                            positive_word_count)
        calculated_negative_probability_value = calculate_bayes_probability(get_amount_of_negative_words(word_lowered),
                                                                            negative_word_count)
        # except Word.DoesNotExist:
        #     pass

        # print(word + " | " + calculated_positive_probability_value.__str__())
        # print(word + " | " + calculated_negative_probability_value.__str__())

        positive_tag_probability *= calculated_positive_probability_value
        negative_tag_probability *= calculated_negative_probability_value

    print("===============================================")
    print("Positive : ")
    print(positive_tag_probability)
    print("<><><><><>")
    print("Negative : ")
    print(negative_tag_probability)
    print("===============================================")
    # tag_result = "Positive" if positive_tag_probability > negative_tag_probability else "Negative"

    tag_result['positive'] = positive_tag_probability
    tag_result['negative'] = negative_tag_probability

    return tag_result


# This function takes a list of tweets and will return the tag whether those tweets are negative or positive
# Example :
# list_of_tweets = ["aku senang", "aku sangat senang"]
# return Positive
# The return data type is String so that it can be scaled (add more tag later)
def get_tag_from_list_of_tweets(list_of_tweets):
    positive_probability = 1
    negative_probability = 1
    for tweet in list_of_tweets:
        probabilities = calculate_sentence_tag_probability(tweet)
        positive_probability *= probabilities['positive']
        negative_probability *= probabilities['negative']
        # experimental_learn(probabilities['tweet_word_list'], positive_probability, negative_probability) # Experimental Learn

    if (positive_probability < negative_probability):
        result = "negative"
    else:
        result = "positive"

    return result


def get_following_and_probability(username):
    # counter = 1
    jsondata = {"friends": []}
    list_of_following_and_probability = []
    list_of_followings = get_user_following(username)
    for following in list_of_followings:
        following_sentiment = get_tag_from_list_of_tweets(get_user_tweet(following))
        user_object = {
            "username": following,
            "tag": following_sentiment
        }

        print(following + " : " + following_sentiment)
        # jsondata["friend"+str(counter)] = [following, following_sentiment]
        jsondata["friends"].append(user_object)
        # counter += 1
    return JsonResponse(jsondata)


def get_one_following_and_probability(request, username_following):
    list_of_tweets = get_user_tweet(username_following)
    jsondata = {"tag" : "", "tweets" : list_of_tweets}
    jsondata["tag"] += get_tag_from_list_of_tweets(list_of_tweets)
    return JsonResponse(jsondata)


def API_get_following_and_probability(request, username):
    return get_following_and_probability(username)


def mock_API_get_following_and_probability(request):
    mock = {
        "friends": [
            {"username": "bejo", "tag": "positive"},
            {"username": "bambang", "tag": "negative"},
            {"username": "sukijan", "tag": "positive"},
        ]
    }
    return JsonResponse(mock)


def process_json_following_and_probablity(following, following_sentiment):
    json_data = {
        "username": following,
        "tag": following_sentiment
    }
    return (json.dumps(json_data))


def api_list(request):
    print(request.POST)
    print(type(request.POST))

    return render(request, 'init.html')


def add_Word_directly_to_database(word, tag, new):
    if new:
        print(">>BARU")
        word_obj = Word(word=word, tag=tag, count=1)
        word_obj.save()

    else:
        print(">>TAMBAH")
        word_obj = Word.objects.get(word=word, tag=tag)
        word_obj.count += 1
        word_obj.save()


# Dont this in production
def store_tweets_data(request):
    # Safety Lock
    password = "hatihati"
    print("[!!WARNING!!]")
    print("Safety Lock, dont run this in production")
    inpt = input("Password> ")
    print("MASUKAN:" + inpt)
    if inpt != password:
        return HttpResponse("Wrong Password")

    # stop_words = set(stopwords.words('english'))
    result = []
    # Because of HEROKU postgres 10000 row limit, we only learn from 500 positive tweet and 500 negative tweet
    # csvfile = open('./static/data/sentimentPOS.csv', newline='').readlines()[1:500]
    # tweets_reader = csv.reader(csvfile, delimiter=',')
    # for row in tweets_reader:
    #     tweet = row[-1]
    #     tag = row[0]
    #     sentence = "".join(tweet)
    #     cleaned_tweet_words = remove_punctuation(sentence)
    #     for word in cleaned_tweet_words:
    #         cleaned_word = word.lower()
    #         # ini bakal nge print tag sama word, kyk udah di pasang2in
    #         # buat ngecek aja, nanti hrs di delete
    #         # print(tag + " | " + word)
    #         if cleaned_word not in stop_words:
    #
    #             if cleaned_word not in result:
    #                 add_Word_directly_to_database(cleaned_word, tag, new=True)
    #             else:
    #                 add_Word_directly_to_database(cleaned_word, tag, new=False)
    #             result.append(cleaned_word)
    #             # data = {
    #             #     'word': word,
    #             #     'tag': tag,
    #             # }
    #             # requests.post(url="http://127.0.0.1:8000/api/words/add/", data=data)
    print("================")
    print("FINISHED sentimentPOS")
    print("FINISHED sentimentPOS")
    print("FINISHED sentimentPOS")
    print("================")

    # result = []  # RESET Database
    #
    # csvfile = open('./static/data/sentimentNEG.csv', newline='').readlines()[4:500]
    # tweets_reader = csv.reader(csvfile, delimiter=',')
    # for row in tweets_reader:
    #     tweet = row[-1]
    #     tag = row[0]
    #     sentence = "".join(tweet)
    #     cleaned_tweet_words = remove_punctuation(sentence)
    #     for word in cleaned_tweet_words:
    #         cleaned_word = word.lower()
    #         # ini bakal nge print tag sama word, kyk udah di pasang2in
    #         # buat ngecek aja, nanti hrs di delete
    #         # print(tag + " | " + word)
    #         if cleaned_word not in stop_words:
    #
    #             if cleaned_word not in result:
    #                 add_Word_directly_to_database(cleaned_word, tag, new=True)
    #             else:
    #                 add_Word_directly_to_database(cleaned_word, tag, new=False)
    #             result.append(cleaned_word)
    #             # data = {
    #             #     'word': word,
    #             #     'tag': tag,
    #             # }
    #             # requests.post(url="http://127.0.0.1:8000/api/words/add/", data=data)
    return HttpResponse(result.__str__())


# takes a sentence(String) and will return list of word without punctuation
# Example:
# remove_punctuation("Aku!#$@ Senang")
# return ["Aku", "Senang"]
def remove_punctuation(sentence):
    cleaned_word = re.sub('[' + string.punctuation + ']', '', sentence).split()
    return cleaned_word


class WordAllView(mixins.RetrieveModelMixin, mixins.ListModelMixin, viewsets.GenericViewSet, ):
    queryset = Word.objects.all()
    serializer_class = WordSerializer


class WordCountView(mixins.RetrieveModelMixin, mixins.ListModelMixin, viewsets.GenericViewSet):
    serializer_class = WordSerializer

    def get_queryset(self):
        queryset = Word.objects.all()
        search_word = self.request.query_params.get('search-word', None)
        if search_word is not None:
            queryset = queryset.filter(word=search_word)
        else:
            queryset = None
        return queryset


class WordAddView(mixins.CreateModelMixin, mixins.UpdateModelMixin, viewsets.GenericViewSet):
    serializer_class = WordPostSerializer

    def create(self, request):
        print(request.data)
        request_data_dict = request.data

        # try:
        #     request_data_dict = request.data.dict()
        #     print('NO Except!! > ')
        #     print(request_data_dict)
        # except AttributeError:
        #     print('EXCEPT')
        #     pass

        try:
            word_obj = Word.objects.get(word=request_data_dict.get('word').lower(), tag=request_data_dict.get('tag'))
            word_obj.count += 1
            word_obj.save()
            print('YEYEYEYE')  # LOG
        except Word.DoesNotExist:
            print('NONONO')  # LOG
            word_obj = Word(word=request_data_dict.get('word').lower(), tag=request_data_dict.get('tag'), count=1)
            word_obj.save()
            # var = Word.objects.get(pk=request_data_dict.get('word'))
            # print(var)
            return JsonResponse({'status': 'Successfully ADDED {}'.format(request_data_dict.get('word'))})

        return JsonResponse({'status': 'Successfully UPDATED {}'.format(request_data_dict.get('word'))})


# For LOG purpose
from django.db import connection


def coba(request):
    result = get_following_and_probability("BimNari")
    print("<LOG>")
    print(connection.queries)
    print("<ENDOF LOG>")
    return result


@csrf_exempt
def coba_dua(request):
    hasil = "HELLO WORLD"
    if request.method == 'POST':
        return get_following_and_probability(request.POST['username'])
    return HttpResponse(hasil)


@csrf_exempt
def coba_tiga(request):
    hasil = "HELLO WORLD"
    if request.method == 'POST':
        # request.POST <dictionary>
        # {
        #  "list": ["tweet", "twttwtwti"]
        # }

        apiDict = dict(request.POST)
        print(apiDict)
        value = apiDict.values()
        list_of_tweets = list(value)[0]
        respon = {"tag": get_tag_from_list_of_tweets(list_of_tweets)}
        # for i in value:
        #     print(type(i))
        #     print(i)

        # return get_tag_from_list_of_tweets(get_user_tweet(request.POST['username']))
    return JsonResponse(respon)

# def add_ExperimentalWord_directly_to_database(word, tag):
#     # if new:
#     #     print(">>BARU")
#     #     word_obj = Word(word=word, tag=tag, count=1)
#     #     word_obj.save()
#     #
#     # else:
#     #     print(">>TAMBAH")
#     #     word_obj = Word.objects.get(word=word, tag=tag)
#     #     word_obj.count += 1
#     #     word_obj.save()
#     try:
#         print(">>TAMBAH EXPERIMENTAL")
#         word_obj = ExperimentalWord.objects.get(word=word, tag=tag)
#         word_obj.count += 1
#         word_obj.save()
#     except ExperimentalWord.DoesNotExist:
#         print(">>BARU EXPERIMENTAL")
#         word_obj = ExperimentalWord(word=word, tag=tag, count=1)
#         word_obj.save()


# Experimental Learn TODO
# def experimental_learn(list_of_words, positive_probability, negative_probability):
#      if positive_probability > negative_probability:
#          for word in list_of_words:
#              add_ExperimentalWord_directly_to_database(word, "positive")
#      else:
#         for word in list_of_words:
#             add_ExperimentalWord_directly_to_database(word, "negative")
#
# def see_experimental_words(request):
#     return HttpResponse(ExperimentalWord.objects.all())



@csrf_exempt
def api_get_tag_from_list_of_tweets(request):
    hasil = "HELLO WORLD"
    if request.method == 'POST':
        # request.POST <dictionary>
        # {
        #  "list": ["tweet", "twttwtwti"]
        # }

        apiDict = dict(request.POST)
        print(apiDict)
        value = apiDict.values()
        list_of_tweets = list(value)[0]
        respon = {"tag": get_tag_from_list_of_tweets(list_of_tweets)}
        # for i in value:
        #     print(type(i))
        #     print(i)

        # return get_tag_from_list_of_tweets(get_user_tweet(request.POST['username']))
    return JsonResponse(respon)

"""
Bagian untuk statistik akurasi
"""
def API_get_tag_from_sentence(request, sentence):
    respon = {"tag" : get_tag_from_sentence(sentence)}
    return JsonResponse(respon)

def get_tag_from_sentence(sentence):
    print(str(sentence))
    positive_probability = 1
    negative_probability = 1
    probabilities = calculate_sentence_tag_probability(sentence)
    positive_probability *= probabilities['positive']
    negative_probability *= probabilities['negative']

    if (positive_probability < negative_probability):
        result = "negative"
    else:
        result = "positive"

    return result

def accuracy_checker(request, firstRow, lastRow):
    benar=0
    miss=0
    error=0
    missInPos=0
    missInNeg=0
    csvfile = open('./static/data/sentimentPOS.csv', newline='').readlines()[int(firstRow):int(lastRow)]
    tweets_reader = csv.reader(csvfile, delimiter=',')
    for row in tweets_reader:
        try:
            tweet = row[-1]
            tag = row[0]
            ml_tag = get_tag_from_sentence(tweet)
            if (tag == "1" and ml_tag == "positive"):
                benar += 1
            elif (tag == "1" and ml_tag == "negative"):
                miss +=1
                missInPos += 1
        except:
            error += 1
            print(row[-1])
        print("benar : " + str(benar)+ " miss : " + str(miss) + " error : "+str(error))
        
    
    csvfile2 = open('./static/data/sentimentNEG.csv', newline='').readlines()[int(firstRow):int(lastRow)]
    tweets_reader2 = csv.reader(csvfile2, delimiter=',')
    for row in tweets_reader2:
        try:
            tweet = row[-1]
            tag = row[0]
            ml_tag = get_tag_from_sentence(tweet)
            if (tag == "0" and ml_tag == "positive"):
                miss += 1
                missInNeg += 1
            elif (tag == "0" and ml_tag == "negative"):
                benar +=1
        except:
            error += 1
            print(row[-1])
        print("benar : " + str(benar)+ " miss : " + str(miss) + " error : "+str(error))
    
    respon = {
        "benar" : benar,
        "miss"  : miss,
        "error" : error,
        "missInPos" : missInPos,
        "missInNeg" : missInNeg
        }
    
    return JsonResponse(respon)

def random_twitter_list(list_tweet, arrSize):
    random_list = []
    list_tweet_size = len(list_tweet)-1
    for i in range(arrSize):
        data = list_tweet[randint(0, list_tweet_size)]
        random_list.append(data)
    print (random_list)
    return random_list


def accuracy_checker_random(request, firstRow, lastRow, arrSize, randomCount):
    benar=0
    miss=0
    error=0
    missInPos=0
    missInNeg=0
    csvfile = open('./static/data/sentimentPOS.csv', newline='').readlines()[int(firstRow):int(lastRow)]
    tweets_reader = csv.reader(csvfile, delimiter=',')
    test = []
    for x in tweets_reader:
        print(x[-1])
        test.append(x[-1])

    for i in range(randomCount):
        list_tweet = []
        list_tweet = random_twitter_list(test, arrSize)
        ml_tag = get_tag_from_list_of_tweets(list_tweet)
        if (ml_tag == "positive"):
            benar += 1
        elif (ml_tag == "negative"):
            miss += 1
            missInPos += 1
    
    csvfile2 = open('./static/data/sentimentNEG.csv', newline='').readlines()[int(firstRow):int(lastRow)]
    tweets_reader2 = csv.reader(csvfile2, delimiter=',')
    test2 = []
    for x in tweets_reader2:
        print(x[-1])
        test2.append(x[-1])

    for i in range(randomCount):
        list_tweet = []
        list_tweet = random_twitter_list(test2, arrSize)
        ml_tag = get_tag_from_list_of_tweets(list_tweet)
        if (ml_tag == "positive"):
            miss += 1
            missInNeg += 1
        elif (ml_tag == "negative"):
            benar += 1
    print("benar : " + str(benar)+ " miss : " + str(miss) + " error : "+str(error))
    
    respon = {
        "benar" : benar,
        "miss"  : miss,
        "missInPos" : missInPos,
        "missInNeg" : missInNeg,
        "listSize": arrSize,
        "randomCounter" : randomCount

        }
    
    return JsonResponse(respon)
