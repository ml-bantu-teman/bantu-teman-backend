import requests
from multiprocessing import Pool
import time

def checker(first, last):
    firstRow = first
    lastRow = last
    benar=0
    miss=0
    missInPos=0
    missInNeg=0
    error=0
    total=0
    for i in range (20):
        r = requests.get("https://api-bantuteman.herokuapp.com/accuracy-checker/"+str(firstRow)+"/"+str(lastRow))
        firstRow += 50
        lastRow += 50
        benar += r.json()['benar']
        miss += r.json()['miss']
        missInPos += r.json()['missInPos']
        missInNeg += r.json()['missInNeg']
        error += r.json()['error']
        print("alive")

    total = benar+miss+error
    return [benar, miss, missInPos, missInNeg, error, total]
if __name__ == "__main__":
    before = time.time()
    with Pool(processes=4) as pool:
        first=700
        last=750
        beda=(20*50)

        r1 = pool.apply_async(checker, (first,last))
        r2 = pool.apply_async(checker, (first+(beda),last+(beda)))
        r3 = pool.apply_async(checker, (first+(2*beda),last+(2*beda)))
        r4 = pool.apply_async(checker, (first+(3*beda),last+(3*beda)))
        r5 = pool.apply_async(checker, (first+(4*beda),last+(4*beda)))



        getter1 = r1.get()
        getter2 = r2.get()
        getter3 = r3.get()
        getter4 = r4.get()
        getter5 = r5.get()

        benar     = getter1[0] + getter2[0] + getter3[0] + getter4[0] + getter5[0]
        miss      = getter1[1] + getter2[1] + getter3[1] + getter4[1] + getter5[1]
        missInPos = getter1[2] + getter2[2] + getter3[2] + getter4[2] + getter5[2]
        missInNeg = getter1[3] + getter2[3] + getter3[3] + getter4[3] + getter5[3]
        error     = getter1[4] + getter2[4] + getter3[4] + getter4[4] + getter5[4]
        total     = getter1[5] + getter2[5] + getter3[5] + getter4[5] + getter5[5]

        print("\n--------------- hasil ---------------")
        print("total           : " + str(total))
        print("benar           : " + str(benar))
        print("miss            : " + str(miss))
        print("miss in pos     : " + str(missInPos))
        print("miss in neg     : " + str(missInNeg))
        print("error           : " + str(error))
        print("accuracy        : " + str(benar/total*100) + "%")

    after = time.time()
    print("\nexecution time  : " + str(after-before))