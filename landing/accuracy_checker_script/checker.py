import requests
import time

before = time.time()
firstRow = 700
lastRow = 750
benar=0
miss=0
missInPos=0
missInNeg=0
error=0
total=0
for i in range (10):
    r = requests.get("https://api-bantuteman.herokuapp.com/accuracy-checker/"+str(firstRow)+"/"+str(lastRow))
    print(r.json())
    firstRow += 50
    lastRow += 50
    print(str(firstRow),str(lastRow))
    benar += r.json()['benar']
    miss += r.json()['miss']
    missInPos += r.json()['missInPos']
    missInNeg += r.json()['missInNeg']
    error += r.json()['error']

total = benar+miss+error
print("\n--------------- hasil ---------------")
print("total           : " + str(total))
print("benar           : " + str(benar))
print("miss            : " + str(miss))
print("miss in pos     : " + str(missInPos))
print("miss in neg     : " + str(missInNeg))
print("error           : " + str(error))
print("accuracy        : " + str(benar/total*100) + "%")
after = time.time()
print("execution time  : " + str(after-before))