from django.db import models


# Create your models here.

class Word(models.Model):
    word = models.CharField(max_length=255)
    count = models.IntegerField()
    tag = models.CharField(max_length=10)

    class Meta:
        unique_together = (('word', 'tag'),)

    def __str__(self):
        return '{} : {}'.format(self.word, self.count)

class ExperimentalWord(models.Model):
    word = models.CharField(max_length=255)
    count = models.IntegerField()
    tag = models.CharField(max_length=10)

    class Meta:
        unique_together = (('word', 'tag'),)

    def __str__(self):
        return '{} : {}'.format(self.word, self.count)